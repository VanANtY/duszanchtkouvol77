﻿using UnityEngine;
namespace NewtonsBall
{
    public class DistanceJointVisualizer : MonoBehaviour
    {
        [SerializeField] private float lineWidth;
        [SerializeField] private Material lineMaterial;
        [SerializeField] private Color lineColor;

        private DistanceJoint2D distanceJoint;
        private LineRenderer lineRenderer;

        private void Start()
        {
            distanceJoint = GetComponent<DistanceJoint2D>();

            lineRenderer = gameObject.AddComponent<LineRenderer>();
            lineRenderer.startColor = lineColor;
            lineRenderer.endColor = lineColor;
            lineRenderer.startWidth = lineWidth;
            lineRenderer.endWidth = lineWidth;
            lineRenderer.material = lineMaterial;

            UpdatePointA();
            UpdatePointB();
        }
        
        private void Update()
        {
            UpdatePointA();
        }

        private void UpdatePointA()
        {
            lineRenderer.SetPosition(0, transform.position);
        }

        private void UpdatePointB()
        {
            lineRenderer.SetPosition(1, distanceJoint.connectedBody.position + distanceJoint.connectedAnchor * 50f);
        }
    }
}