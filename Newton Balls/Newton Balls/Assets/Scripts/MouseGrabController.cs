﻿using UnityEngine;
namespace NewtonsBall
{
    public class MouseGrabController : MonoBehaviour
    {
        private Rigidbody2D rigidBody;
        private bool followCursorPosition;

        private void OnMouseDown()
        {
            EnableDragging();
        }

        private void OnMouseUp()
        {
            DisableDragging();
        }

        private void Start()
        {
            rigidBody = GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            if (followCursorPosition)
            {
                rigidBody.MovePosition(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            }
        }

        private void EnableDragging()
        {
            rigidBody.bodyType = RigidbodyType2D.Kinematic;
            followCursorPosition = true;
        }

        private void DisableDragging()
        {
            rigidBody.bodyType = RigidbodyType2D.Dynamic;
            followCursorPosition = false;
        }
    }
}