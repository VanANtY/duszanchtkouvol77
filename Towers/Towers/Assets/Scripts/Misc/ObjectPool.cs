﻿using System.Collections.Generic;
using UnityEngine;

namespace Towers.Misc
{
    public class ObjectPool : MonoBehaviour
    {
        [SerializeField]
        private GameObject prefabBase;

        private List<GameObject> objectPool;

        public void Initialize(GameObject prefab)
        {
            prefabBase = prefab;
        } 

        public GameObject Borrow()
        {
            GameObject temp;

            if (objectPool.Count > 0)
            {
                temp = objectPool[0];
                objectPool.Remove(temp);
                temp.SetActive(true);
            }
            else
            {
                temp = GameObject.Instantiate(prefabBase);
            }

            temp.transform.SetParent(null);
            return temp;
        }

        public void Return(GameObject objectToReturn)
        {
            objectToReturn.SetActive(false);
            objectToReturn.transform.SetParent(transform);
            objectPool.Add(objectToReturn);
        }

        private void Awake()
        {
            objectPool = new List<GameObject>();
        }
    }
}