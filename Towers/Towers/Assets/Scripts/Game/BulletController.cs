﻿using UnityEngine;

namespace Towers.Game
{
    public class BulletController : MonoBehaviour
    {
        [SerializeField] private Vector2 bulletRange;
        [SerializeField] private float bulletSpeed;

        private float distanceTravelled;
        private float actualRange;
        private Vector3 direction;

        public void Initialize(Vector3 direction)
        {
            this.direction = direction;
            distanceTravelled = 0;
            actualRange = Random.Range(bulletRange.x, bulletRange.y);
        } 
        
        private void Update()
        {
            var nextPosition = Vector3.MoveTowards(transform.position, transform.position + direction, bulletSpeed * Time.deltaTime);
            distanceTravelled += Vector3.Distance(transform.position, nextPosition);
            transform.position = nextPosition;

            if (distanceTravelled > actualRange)
            {
                BulletDestroy();

                if (!GameController.Instance.LimitReached)
                    SpawnTower();
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "Tower")
            {
                var tower = collision.gameObject.GetComponent<TowerController>();
                tower.BulletHit();
                BulletDestroy();
            }
        }

        private void SpawnTower()
        {
            var tower = GameController.Instance.towersPool.Borrow();
            tower.transform.position = transform.position;
            tower.GetComponent<TowerController>().Initialize(false);
        }

        private void BulletDestroy()
        {
            GameController.Instance.bulletsPool.Return(gameObject);
        }
    }
}