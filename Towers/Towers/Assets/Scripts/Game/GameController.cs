﻿using UnityEngine;
using System.Collections.Generic;
using Towers.Misc;

namespace Towers.Game
{
    public class GameController : MonoBehaviour
    {
        public static GameController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GameObject("Game Controller").AddComponent<GameController>();
                }
                return instance;
            }
        }
        private static GameController instance;

        public ObjectPool towersPool;
        public ObjectPool bulletsPool;

        public bool LimitReached { get { return activeTowers.Count >= 100; } }

        private List<TowerController> activeTowers;

        public void RegisterTower(TowerController tower)
        {
            activeTowers.Add(tower);

            if (activeTowers.Count >= 100)
            {
                RestartAllTowers();
            }
        }

        public void UnregisterTower(TowerController tower)
        {
            activeTowers.Remove(tower);
        }

        private void RestartAllTowers()
        {
            foreach (var tower in activeTowers)
            {
                tower.Restart();
            }
        }

        private void Awake()
        {
            instance = this;
            activeTowers = new List<TowerController>();
        }

        private void OnGUI()
        {
            GUI.Label(new Rect(10, 10, 200, 60), string.Format("Towers: {0}", activeTowers.Count));
        }
    }
}