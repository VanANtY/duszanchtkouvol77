﻿using UnityEngine;

namespace Towers.Game
{
    public class TowerController : MonoBehaviour
    {
        [SerializeField] private float turnCooldown;
        [SerializeField] private Vector2 turnRange;
        [SerializeField] private int baseAmmo;
        [SerializeField] private Color inactiveColor;
        [SerializeField] private Color deployedColor;
        [SerializeField] private float deployTime;
        [SerializeField] private bool deployed;

        private SpriteRenderer spriteRenderer;
        private GameObject cannon;
        private Transform bulletSpawnPoint;
        private float cannonAngle;
        private float lastTurnStamp;
        private float spawnTime;
        private int currentAmmo;

        public void Restart()
        {
            currentAmmo = baseAmmo;
            deployed = true;
            UpdateColor();
        }

        public void Initialize(bool deployedInstantly)
        {
            currentAmmo = baseAmmo;
            deployed = deployedInstantly;
            cannonAngle = 0f;
            lastTurnStamp = Time.time;
            spawnTime = Time.time;

            UpdateColor();
            GameController.Instance.RegisterTower(this);
        }

        public void BulletHit()
        {
            GameController.Instance.towersPool.Return(gameObject);
            GameController.Instance.UnregisterTower(this);
        }

        private void Start()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            cannon = transform.GetChild(0).gameObject;
            bulletSpawnPoint = cannon.transform.GetChild(0);

            Initialize(deployed);
        }

        private void Update()
        {
            if (deployed)
            {
                if (Time.time > lastTurnStamp + turnCooldown && currentAmmo > 0)
                {
                    Turn();
                    Shoot();
                }
                else if (currentAmmo <= 0)
                {
                    UpdateColor();
                }
            }
            else if (Time.time > spawnTime + deployTime)
            {
                deployed = true;
                UpdateColor();
            }
        }

        private void Turn()
        {
            lastTurnStamp = Time.time;
            cannonAngle = Random.Range(turnRange.x, turnRange.y) * ((Random.Range(0,100) > 50) ? 1 : -1);
            cannon.transform.RotateAround(transform.position, Vector3.forward, cannonAngle);
        }

        private void Shoot()
        {
            var bullet = GameController.Instance.bulletsPool.Borrow();
            bullet.transform.position = bulletSpawnPoint.position;
            bullet.GetComponent<BulletController>().Initialize(bulletSpawnPoint.position - transform.position);
            currentAmmo--;
        }

        private void UpdateColor()
        {
            if (deployed && currentAmmo > 0)
            {
                spriteRenderer.color = deployedColor;
            }
            else
            {
                spriteRenderer.color = inactiveColor;
            }
        }
    }
}